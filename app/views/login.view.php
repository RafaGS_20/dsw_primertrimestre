<!------------------------------------------------------------   Vista a mostrar.   ------------------------------------------------------------>


<!-- Incluimos contenido por partials -->
<?php require_once __DIR__ . "/partials/head.php";  // Llamamos al head. ?>
<?php require_once __DIR__ . "/partials/nav.php"; // Llamamos al nav. ?>


<!------------------------------------------------------------  INICIO DEL CÓDIGO HTML.   ------------------------------------------------------------>
<div id="contact">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1> Conectate. </h1>
            <hr>
            <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') : ?>
                <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
                    <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <?php if (empty($errores)) : ?>
                        <p><?= $mensaje ?></p>
                    <?php else : ?>
                        <ul>
                            <?php foreach ($errores as $error) : ?>
                                <li><?= $error ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <form class="form-horizontal" method="POST">
                <div class="form-group">
                    <div class="col-xs-6">
                        <label class="label-control">Nombre:</label>
                        <input class="form-control" type="text" name="nombre" value="">
                    </div>
                    <div class="col-xs-6">
                        <label class="label-control">Contraseña:</label>
                        <input class="form-control" type="password" name="contraseña" value="">
                    </div>
                </div>
                <button type="submit" class="site-btn">Enviar</button>
                <br>
                <br>
                <br>
            </form>
        </div>


<!------------------------------------------------------------  FIN DEL CÓDIGO HTML.   ------------------------------------------------------------>

<!-- Incluimos contenido por partials -->
<?php require_once __DIR__ . "/partials/footer.php"; // Llamamos al footer. ?>
<?php require_once __DIR__ . "/partials/fin-doc.php"; // Llamamos a los scripts. ?>