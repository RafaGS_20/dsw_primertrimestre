<!------------------------------------------------------------   Vista a mostrar.   ------------------------------------------------------------>


<!-- Incluimos contenido por partials -->
<?php require_once __DIR__ . "/partials/head.php";  // Llamamos al head. ?>
<?php require_once __DIR__ . "/partials/nav.php"; // Llamamos al nav. ?>


<!------------------------------------------------------------  INICIO DEL CÓDIGO HTML.   ------------------------------------------------------------>

<div id="galeria">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1>GALERÍA</h1>
            <hr>
            <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') : ?>
                <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
                    <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <?php if (empty($errores)) : ?>
                        <p><?= $mensaje ?></p>
                    <?php else : ?>
                        <ul>
                            <?php foreach ($errores as $error) : ?>
                                <li><?= $error ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

            <form class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Imagen</label>
                        <input class="form-control-file" name="imagen" type="file">
                    </div>
                </div>
                <div class="col-xs-12">
                    <label class="label-control"> Categoría </label>

                    <select class="form-control" name="categoria">
                        <?php foreach ($categorias as $categoria) : ?>

                            <option value="<?= $categoria->getId() ?>"

                            <?= $categoriaSeleccionada ==  $categoria->getId() ? 'selected' : '' ?>

                            >

                            <?= $categoria->getNombre();?></option>

                        <?php endforeach; ?>
                    </select><br><br>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Descripción</label>
                        <input class="form-control" type="text" name="descripcion" value=""> <br><br>
                        <button type="submit" class="site-btn">Enviar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col">
                <table style="border: 2px solid black;" class="table">
                    <th style="background-color: grey; color:white" scope="col"> ID </th>
                    <th style="background-color: grey; color:white" scope="col"> Imagen </th>
                    <th style="background-color: grey; color:white" scope="col"> Descripción </th>
                    <th style="background-color: grey; color:white" scope="col"> Categoría </th>

                    <?php if (isset($imagenes)) {
                    ?>

                        <?php foreach ($imagenes as $imagen) : ?>

                            <tr>
                                <th scope="row"><?= $imagen->getId() ?></th>
                                <td>
                                    <img src="<?= $imagen->getURLGallery() ?>" alt="" title="<?= $imagen->getNombre() ?>" width="15%">
                                </td>
                                <td><?= $imagen->getDescripcion() ?></td>
                                <td><?= $imagenPaginaRepository->getCategoria($imagen)->getNombre() ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
</div>
 

<!------------------------------------------------------------  FIN DEL CÓDIGO HTML.   ------------------------------------------------------------>

<!-- Incluimos contenido por partials -->
<?php require_once __DIR__ . "/partials/footer.php"; // Llamamos al footer. ?>
<?php require_once __DIR__ . "/partials/fin-doc.php"; // Llamamos a los scripts. ?>