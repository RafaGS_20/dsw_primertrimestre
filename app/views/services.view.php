<!------------------------------------------------------------   Vista a mostrar.   ------------------------------------------------------------>


<!-- Incluimos contenido por partials -->
<?php require_once __DIR__ . "/partials/head.php";  // Llamamos al head. ?>
<?php require_once __DIR__ . "/partials/nav.php"; // Llamamos al nav. ?>


<!------------------------------------------------------------  INICIO DEL CÓDIGO HTML.   ------------------------------------------------------------>

    <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bo-links">
                        <a href="./index.html"><i class="fa fa-home"></i> Home</a>
                        <span>Servicios</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

<!-- Services Section Begin -->
<section class="services-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="services-item">
                    <img src="img/services/service-1.jpg" alt="">
                    <h3>Fotografía</h3>
                    <br>
                    <p> Tendrás a tu disposición a los mejores fotógrafos para inmortalizar cualquier momento que desees.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="services-item">
                    <img src="img/services/service-2.jpg" alt="">
                    <h3>Videos</h3>
                    <p> También podrás contratar servicios de grabación en vídeo para revivir ese momento tan especial para
                        siempre que quieras.
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="services-item">
                    <img src="img/services/service-3.jpg" alt="">
                    <h3>Edición</h3>
                    <p>Todas nuestras imágenes son tomadas en formato RAW y editadas posteriormente para darte la mejor calidad de imágen.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Services Section End -->

    <!-- Services Option Section Begin -->
    <section class="services-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="so-item">
                        <div class="so-title">
                            <div class="so-number">01</div>
                            <h5>Filming and Editing</h5>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
                    </div>
                    <div class="so-item">
                        <div class="so-title">
                            <div class="so-number">02</div>
                            <h5>Engagement photography</h5>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
                    </div>
                    <div class="so-item">
                        <div class="so-title">
                            <div class="so-number">03</div>
                            <h5>Comercial photography</h5>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="so-item">
                        <div class="so-title">
                            <div class="so-number">04</div>
                            <h5>Social media photography</h5>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
                    </div>
                    <div class="so-item">
                        <div class="so-title">
                            <div class="so-number">02</div>
                            <h5>Event Photography</h5>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
                    </div>
                    <div class="so-item">
                        <div class="so-title">
                            <div class="so-number">03</div>
                            <h5>personal photography</h5>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Services Option Section End -->

    <!------------------------------------------------------------  FIN DEL CÓDIGO HTML.   ------------------------------------------------------------>

<!-- Incluimos contenido por partials -->
<?php require_once __DIR__ . "/partials/footer.php"; // Llamamos al footer. ?>
<?php require_once __DIR__ . "/partials/fin-doc.php"; // Llamamos a los scripts. ?>