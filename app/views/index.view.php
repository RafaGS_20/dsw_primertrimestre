<!------------------------------------------------------------   Vista a mostrar.   ------------------------------------------------------------>


<!-- Incluimos contenido por partials -->
<?php require_once __DIR__ . "/partials/head.php";  // Llamamos al head. 
?>
<?php require_once __DIR__ . "/partials/nav.php"; // Llamamos al nav. 
?>


<!------------------------------------------------------------  INICIO DEL CÓDIGO HTML.   ------------------------------------------------------------>

<!-- Hero Section Begin -->
<section class="hero-section">
    <div class="hs-slider owl-carousel">
        <div class="hs-item set-bg" data-setbg="img/hero/hero-1.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="hs-text">
                            <h2>Photo-Rafa</h2>
                            <p> Disfruta de la mejor experiencia posible para cualquier circunstancia con nuestros servicios fotográficos.<br />
                                Disponible para toda clase de eventos y situaciones.</p>
                            <a href="consultas" class="primary-btn">Contacta con nosotros</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hs-item set-bg" data-setbg="img/hero/hero-2.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="hs-text">
                            <h2>Photo-Rafa</h2>
                            <p> Disfruta de la mejor experiencia posible para cualquier circunstancia con nuestros servicios fotográficos.<br />
                                Disponible para toda clase de eventos y situaciones.</p>
                            <a href="consultas" class="primary-btn">Contacta con nosotros</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Hero Section End -->

<!-- Services Section Begin -->
<section class="services-section spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="services-item">
                    <img src="img/services/service-1.jpg" alt="">
                    <h3>Fotografía</h3>
                    <br>
                    <p> Tendrás a tu disposición a los mejores fotógrafos para inmortalizar cualquier momento que desees.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="services-item">
                    <img src="img/services/service-2.jpg" alt="">
                    <h3>Videos</h3>
                    <p> También podrás contratar servicios de grabación en vídeo para revivir ese momento tan especial para
                        siempre que quieras.
                    </p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="services-item">
                    <img src="img/services/service-3.jpg" alt="">
                    <h3>Edición</h3>
                    <p>Todas nuestras imágenes son tomadas en formato RAW y editadas posteriormente para darte la mejor calidad de imágen.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Services Section End -->


<!------------------------------------------------------------  FIN DEL CÓDIGO HTML.   ------------------------------------------------------------>

<!-- Incluimos contenido por partials -->
<?php require_once __DIR__ . "/partials/footer.php"; // Llamamos al footer. 
?>
<?php require_once __DIR__ . "/partials/fin-doc.php"; // Llamamos a los scripts. 
?>