<!------------------------------------------------------------   Vista a mostrar.   ------------------------------------------------------------>


<!-- Incluimos contenido por partials -->
<?php require_once __DIR__ . "/partials/head.php";  // Llamamos al head. 
?>
<?php require_once __DIR__ . "/partials/nav.php"; // Llamamos al nav. 
?>


<!------------------------------------------------------------  INICIO DEL CÓDIGO HTML.   ------------------------------------------------------------>

<!-- Inicio del formulario para el envío de consultas. -->

<div id="contact" style="justify-content: center;">
    <div class="container">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
            <h1> Envíanos tu consulta. </h1>
            <hr>
            <p> Las atenderemos y te enviaremos la respuesta lo más pronto posible.</p>
            <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') : ?>
                <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
                    <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <?php if (empty($errores)) : ?>
                        <p><?= $mensaje ?></p>
                    <?php else : ?>
                        <ul>
                            <?php foreach ($errores as $error) : ?>
                                <li><?= $error ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
            <form class="form-horizontal" method="POST">
                <div class="form-group">
                    <div class="col-xs-6">
                        <label class="label-control">Nombre:</label>
                        <input class="form-control" type="text" name="nombre" value="">
                    </div>
                    <div class="col-xs-6">
                        <label class="label-control">Apellidos:</label>
                        <input class="form-control" type="text" name="apellidos" value="">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Email:</label>
                        <input class="form-control" type="email" name="email" value="">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Asunto:</label>
                        <input class="form-control" type="text" name="asunto" value="">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="label-control">Pregunta:</label>
                        <input class="form-control" type="text" name="pregunta" value="">
                    </div>
                </div>
                <button type="submit" class="site-btn">Enviar</button>
            </form>
        </div>


        <!-- Fin del formulario para el envío de consultas. -->

        <!-- Services Option Section Begin -->
        <section class="services-option">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="section-title">
                            <h2>frequently asked questions</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="so-item">
                            <div class="so-title">
                                <div class="so-number">01</div>
                                <h5>Filming and Editing</h5>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
                        </div>
                        <div class="so-item">
                            <div class="so-title">
                                <div class="so-number">02</div>
                                <h5>Engagement photography</h5>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
                        </div>
                        <div class="so-item">
                            <div class="so-title">
                                <div class="so-number">03</div>
                                <h5>Comercial photography</h5>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="so-item">
                            <div class="so-title">
                                <div class="so-number">04</div>
                                <h5>Social media photography</h5>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
                        </div>
                        <div class="so-item">
                            <div class="so-title">
                                <div class="so-number">02</div>
                                <h5>Event Photography</h5>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
                        </div>
                        <div class="so-item">
                            <div class="so-title">
                                <div class="so-number">03</div>
                                <h5>personal photography</h5>
                            </div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                magna aliqua. Quis ipsum suspendisse ultrices gravida.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Services Option Section End -->

        <!------------------------------------------------------------  FIN DEL CÓDIGO HTML.   ------------------------------------------------------------>

        <!-- Incluimos contenido por partials -->
        <?php require_once __DIR__ . "/partials/footer.php"; // Llamamos al footer. 
        ?>
        <?php require_once __DIR__ . "/partials/fin-doc.php"; // Llamamos a los scripts. 
        ?>