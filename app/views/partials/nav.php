<!-- Partials creado para extraer el nav de nuestras vistas e insertarlo de forma rápida. -->

<?php include "/var/www/html/DSW_PrimerTrimestre/utils/utils.php"; ?>

<!-- Header Section Begin -->
<header class="header-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="logo">
                        <a href="index">
                            <img src="img/logo.png" alt="">
                        </a>
                    </div>
                    <nav class="nav-menu mobile-menu">
                        <ul>
                            <li class="<?php if ( esactiva("index") ) echo "active"; ?>"><a href="<?php if ( esactiva("index") ) echo "#"; else echo "index"; ?>">Inicio</a></li>
                            <li class="<?php if ( esactiva("about") ) echo "active"; ?>"><a href="<?php if ( esactiva("about") ) echo "#"; else echo "about"; ?>">Sobre nosotros</a></li>
                            <li class="<?php if ( esactiva("services") ) echo "active"; ?>"><a href="<?php if ( esactiva("services") ) echo "#"; else echo "services"; ?>">Servicios</a></li>
                            <li class="<?php if ( esactiva("consultas") ) echo "active"; ?>"><a href="<?php if ( esactiva("consultas") ) echo "#"; else echo "consultas"; ?>">Consultas</a></li>
                            <li class="<?php if ( esactiva("portfolio") ) echo "active"; ?>"><a href="<?php if ( esactiva("portfolio") ) echo "#"; else echo "portfolio"; ?>">Portfolio</a></li>
                            <li class="<?php if ( esactiva("subida") ) echo "active"; ?>"><a href="<?php if ( esactiva("subida") ) echo "#"; else echo "subida"; ?>">Upload</a></li>
                            <li class="<?php if ( esactiva("login") ) echo "active"; ?>"><a href="<?php if ( esactiva("login") ) echo "#"; else echo "login"; ?>">Login</a></li>
                        </ul>
                    </nav>
                    <div class="top-search search-switch">
                        <i class="fa fa-search"></i>
                    </div>
                    <div id="mobile-menu-wrap"></div>
                </div>
            </div>
        </div>
    </header>
    <!-- Header End -->