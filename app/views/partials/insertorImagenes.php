<!-- Partials creado para extraer el la inserción de imágenes. -->
<?php
shuffle($arrayImagenes);
?>
<!-- Portfolio Section Begin -->
<section class="portfolio-section portfolio-page spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2>Nuestro trabajo</h2>
                </div>
                <div class="filter-controls">
                    <ul>
                        <li class="active" data-filter="*"> Todas </li>
                        <li data-filter=".Estrellas"> Estrellas </li>
                        <li data-filter=".Coches"> Coches </li>
                        <li data-filter=".Paisajes"> Paisajes </li>
                        <li data-filter=".Militares"> Militares </li>
                        <li data-filter=".PC"> PC Setup</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 p-0">
                <div class="portfolio-filter">
                <!-- Este es el código que se repite para cada imagen -->
                <!-- Entramos en un bucle de tamaño del array para que meta el contenido -->
                <?php foreach ($arrayImagenes as $imagen) 
                {
                    ?>
                    <!-- Cerramos php para trabajar el html y que durante todo el bucle use el mismo -->
                    <div class="pf-item set-bg <?= $imagenPaginaRepository->getCategoria($imagen)->getNombre() ?>" data-setbg="<?= $imagen->getURLGallery() ?>">
                        <a href="<?= $imagen->getURLGallery() ?>" class="pf-icon image-popup"><span class="icon_plus"></span></a>
                        <div class="pf-text">
                            <h4><?= $imagen->getNombre() ?></h4>
                            <span><?= $imagen->getDescripcion() ?></span>
                        </div>
                    </div>
                    <!-- Cerramos el foreach de arriba -->
                    <?php
                }?>
                <!-- Fin -->
            </div>
        </div>
    </div>
</section>




