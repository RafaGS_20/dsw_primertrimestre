<!------------------------------------------------------------   Enrutador.   ------------------------------------------------------------>

<?php 

/* Este archivos es un simple array asociativo, para cada clave devuelve un controller que usar
 para acabar sacando una vista, es un enrutador casero. */
return [

 
 "DSW_PrimerTrimestre/index" => "app/controllers/index.php", // Acceso al index por botón.

 "DSW_PrimerTrimestre" => "app/controllers/index.php", // Acceso al index desde inicio al servidor.
 
 "DSW_PrimerTrimestre/about" => "app/controllers/about.php", // Acceso al about.

 "DSW_PrimerTrimestre/login" => "app/controllers/login.php", // Acceso al login.

 "DSW_PrimerTrimestre/portfolio" => "app/controllers/portfolio.php", // Acceso al portfolio.

 "DSW_PrimerTrimestre/subida" => "app/controllers/subida.php", // Acceso al subida.

 "DSW_PrimerTrimestre/consultas" => "app/controllers/consultas.php", // Acceso al consultas.

 "DSW_PrimerTrimestre/services" => "app/controllers/services.php", // Acceso al services.

]

?>