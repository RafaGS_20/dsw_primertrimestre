<!------------------------------------------------------------   Configuración base de datos.   ------------------------------------------------------------>

<?php

/* En este archivos vamos a cargar parte de la configuración de toda nuestra app. 
En este caso cargamos los datos de la base de datos para realizar conexión. */

return [

    "database" => [

        "name" => "proyecto", 

        "username" => "rafa",

        "password" => "123456", 

        "connection" => "mysql:host=localhost", 

        "options" => [
            /* La primera opciones es cambiar el atributo de caracteres de la conexión
            a UTF-8.  */
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            /* La segunda opción es cambiar el modo de error de silent (por defecto) a 
            exception.  */ 
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            /* La tercera nos mantiene la persistencia, la conexión quedará abierta aunque 
            se acabe el script. */
            PDO::ATTR_PERSISTENT => true

        ]

    ]

];