<!------------------------------------------------------------   Controlador del contact.   ------------------------------------------------------------>

<?php

/* -----------------------------------------------------------   Gestión de recursos a usar.   -----------------------------------------------------------*/

// Excepciones: usadas para recoger los diferentes errores en la app. 
require_once __DIR__ ."/../../exceptions/QueryException.php"; // Clase que maneja los errores producidos durante la interacción con la base de datos.
require_once __DIR__ . "/../../exceptions/FileException.php"; // Clase con los fallos de los archivos. 
require_once __DIR__ . "/../../exceptions/AppException.php"; // Clase que maneja los errores producidos por el contenedor de servicios. 
 
// Entity: son nuestras clases principales, contenidas en la página y en la base de datos. 
require_once __DIR__ ."/../../entity/Usuarios.php"; // Clase con los usuarios.

// Repository: gestionan la interacción entre nuestras clases y la base de datos. 
require_once __DIR__ . "/../../repository/UsuariosRepository.php"; // Clase que gestiona la interacción con la base de datos. 

// Database: contiene datos y sentencias de interacción con la base de datos. 
require_once __DIR__ . "/../../database/Connection.php"; // Clase que gestiona la conexión con la base de datos. 
require_once __DIR__ . "/../../database/QueryBuilder.php"; // Clase que gestiona nuestras sentencias sql. 

// Core: Contenedor de servicio y precargas para el funcionamiento.  
require_once __DIR__ . "/../../core/App.php"; // Clase que gestiona el contenedor de servicio. 
require_once __DIR__ . "/../../core/bootstrap.php"; // Clase que gestiona la carga de la base de datos. 
require_once __DIR__ . "/../../core/helpers/FlashMessage.php"; // Clase para la persistencia de datos en sesiones. 

// Monolog: uso del monolog para la gestión de logs en nuestro servicio. 
use Monolog\Logger; // Se usa para la creación y manejos de logs.
use Monolog\Handler\StreamHandler; // Se usa para almacenar recursos en local.  

/* -----------------------------------------------------------   Funcionalidad de la página.   -----------------------------------------------------------*/

// Uso de Monolog en nuestra aplicación. 
$log = new Logger('galeria');
$log->pushHandler(new StreamHandler('logs/info.log', Logger::INFO));

/* -----------------------------------------------------------   Llamada a la vista.   -----------------------------------------------------------*/
require_once __DIR__ . "/../views/login.view.php"; 

?>