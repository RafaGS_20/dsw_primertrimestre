<!------------------------------------------------------------   Controlador del portfolio.   ------------------------------------------------------------>

<?php


/* -----------------------------------------------------------   Gestión de recursos a usar.   -----------------------------------------------------------*/

// Utils: pequeñas inserciones de código con funcionalidades simples para la página. 
require_once __DIR__ . "/../../utils/File.php"; // Clase que gestiona los archivos. 

// Excepciones: usadas para recoger los diferentes errores en la app. 
require_once __DIR__ ."/../../exceptions/QueryException.php"; // Clase que maneja los errores producidos durante la interacción con la base de datos.
require_once __DIR__ . "/../../exceptions/FileException.php"; // Clase con los fallos de los archivos. 
require_once __DIR__ . "/../../exceptions/AppException.php"; // Clase que maneja los errores producidos por el contenedor de servicios. 
 
// Entity: son nuestras clases principales, contenidas en la página y en la base de datos. 
require_once __DIR__ ."/../../entity/ImagenPagina.php"; // Clase con todos los archivos que tenemos.
require_once __DIR__ . "/../../entity/Categoria.php"; // Clase con las categorías. 

// Repository: gestionan la interacción entre nuestras clases y la base de datos. 
require_once __DIR__ . "/../../repository/CategoriaRepository.php"; // Clase que gestiona la interacción con la base de datos. 
require_once __DIR__ . "/../../repository/ImagenPaginaRepository.php"; // Clase que gestiona la interacción con la base de datos. 

// Database: contiene datos y sentencias de interacción con la base de datos. 
require_once __DIR__ . "/../../database/Connection.php"; // Clase que gestiona la conexión con la base de datos. 
require_once __DIR__ . "/../../database/QueryBuilder.php"; // Clase que gestiona nuestras sentencias sql. 

// Core: Contenedor de servicio y precargas para el funcionamiento.  
require_once __DIR__ . "/../../core/App.php"; // Clase que gestiona el contenedor de servicio. 
require_once __DIR__ . "/../../core/bootstrap.php"; // Clase que gestiona la carga de la base de datos. 
require_once __DIR__ . "/../../core/helpers/FlashMessage.php"; // Clase para la persistencia de datos en sesiones. 

/* -----------------------------------------------------------   Funcionalidad de la página.   -----------------------------------------------------------*/

// Array de imágenes a mostrar. 
$arrayImagenes = array();

// Creamos las variables pertinentes para atacar a la base de datos y buscar en ella. 
$imagenPaginaRepository = new ImagenPaginaRepository();
$imagenes = $imagenPaginaRepository->findAll();

// Procedemos a la inserción desde la base de datos a la página. 
for ( $i = 0; $i <= count($imagenes)-1; $i++ ){
    $imagen_BBDD = new ImagenPagina(
        $imagenes[$i]->getId(),
        $imagenes[$i]->getNombre(),
        $imagenes[$i]->getDescripcion(),
        $imagenes[$i]->getCategoria(),
    );
    // Metemos todo en el array. 
    array_push( $arrayImagenes, $imagen_BBDD);
}

/* -----------------------------------------------------------   Llamada a la vista.   -----------------------------------------------------------*/
require_once __DIR__ . "/../views/portfolio.view.php"; 

?>