<!------------------------------------------------------------   Controlador del formulario de consultas.   ------------------------------------------------------------>

<?php

/* -----------------------------------------------------------   Gestión de recursos a usar.   -----------------------------------------------------------*/

// Excepciones: usadas para recoger los diferentes errores en la app. 
require_once __DIR__ ."/../../exceptions/QueryException.php"; // Clase que maneja los errores producidos durante la interacción con la base de datos.
require_once __DIR__ . "/../../exceptions/FileException.php"; // Clase con los fallos de los archivos. 
require_once __DIR__ . "/../../exceptions/AppException.php"; // Clase que maneja los errores producidos por el contenedor de servicios. 
 
// Entity: son nuestras clases principales, contenidas en la página y en la base de datos. 
require_once __DIR__ ."/../../entity/Consulta.php"; // Clase con las consultas.

// Repository: gestionan la interacción entre nuestras clases y la base de datos. 
require_once __DIR__ . "/../../repository/ConsultasRepository.php"; // Clase que gestiona la interacción con la base de datos. 

// Database: contiene datos y sentencias de interacción con la base de datos. 
require_once __DIR__ . "/../../database/Connection.php"; // Clase que gestiona la conexión con la base de datos. 
require_once __DIR__ . "/../../database/QueryBuilder.php"; // Clase que gestiona nuestras sentencias sql. 

// Core: Contenedor de servicio y precargas para el funcionamiento.  
require_once __DIR__ . "/../../core/App.php"; // Clase que gestiona el contenedor de servicio. 
require_once __DIR__ . "/../../core/bootstrap.php"; // Clase que gestiona la carga de la base de datos. 
require_once __DIR__ . "/../../core/helpers/FlashMessage.php"; // Clase para la persistencia de datos en sesiones. 

// Monolog: uso del monolog para la gestión de logs en nuestro servicio. 
use Monolog\Logger; // Se usa para la creación y manejos de logs.
use Monolog\Handler\StreamHandler; // Se usa para almacenar recursos en local.  

/* -----------------------------------------------------------   Funcionalidad de la página.   -----------------------------------------------------------*/

// Uso de Monolog en nuestra aplicación. 
$log = new Logger('galeria');
$log->pushHandler(new StreamHandler('logs/info.log', Logger::INFO));
$mensaje = FlashMessage::get("mensaje");

// Iniciamos un try en el que meteremos el funcionamiento de nuestra página: 
try {

    // Accedemos a nuestra clase creada para gestionar la base de datos y usamos el método de conexión. 
    $consultasRepository = new ConsultasRepository();

    if ($_SERVER['REQUEST_METHOD'] === 'POST')
    {
        // Cogemos los parámetros del post evitando inyecciones de código. 
        $nombre = htmlspecialchars(trim(($_POST['nombre']), " ")) ?? ''; 
        FlashMessage::set("nombre", $nombre);
        $apellidos = htmlspecialchars(trim(($_POST['apellidos']), " ")) ?? ''; 
        FlashMessage::set("apellidos", $apellidos);
        $email = htmlspecialchars(trim(($_POST['email']), " ")) ?? ''; 
        FlashMessage::set("email", $email);
        $asunto = htmlspecialchars(trim(($_POST['asunto']), " ")) ?? ''; 
        FlashMessage::set("asunto", $asunto);
        $pregunta = htmlspecialchars(trim(($_POST['pregunta']), " ")) ?? ''; 
        FlashMessage::set("pregunta", $pregunta);

        // Creamos un objeto consulta con la consulta generada. 
        $consulta = new Consulta($id = 0, $nombre, $apellidos, $asunto, $pregunta, $email);
        // Usamos nuestro query para guardar la consulta en la base de datos.
        $consultasRepository->save($consulta);
        // Mensaje de salida si todo va bien, haciendo uso de sesiones.  
        FlashMessage::set("mensaje", "Su consulta se ha archivado, le responderemos con la mayor brevedad posible.");
        // Se crea un registro en el archivo log.
        $log->info($mensaje);
        App::get("logger")->add($mensaje);
        // Reseteamos los parámetros en caso de realizarse la subida.  
        FlashMessage::unset("nombre", $nombre);
        FlashMessage::unset("apellidos", $apellidos);
        FlashMessage::unset("email", $email);
        FlashMessage::unset("asunto", $asunto);
        FlashMessage::unset("pregunta", $pregunta);
        $mensaje = "";

    }
    

}

/* -----------------------------------------------------------   Captura de errores.   -----------------------------------------------------------*/

catch (FileException $fileException) {

    FlashMessage::set("errores", [$fileException->getMessage()]);
    
}
catch (QueryException $queryException) {

    FlashMessage::set("errores", [$queryException->getMessage()]);
} 
catch (PDOException $pdoException) {

    FlashMessage::set("errores", [$pdoException->getMessage()]);
}
catch (AppException $appException) {

    FlashMessage::set("errores", [$appException->getMessage()]);
}

// Con estas líneas evitaremos que un mismo mensaje se guarde más de una vez por sesión.
$errores = FlashMessage::get("errores");
unset($_SESSION["errores"]);
$mensaje = FlashMessage::get("mensaje");
unset($_SESSION["mensajes"]);


/* -----------------------------------------------------------   Llamada a la vista.   -----------------------------------------------------------*/
require_once __DIR__ . "/../views/consultas.view.php"; 

?>