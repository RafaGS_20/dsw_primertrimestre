<!------------------------------------------------------------   Clase para las categorías de las imágenes.   ------------------------------------------------------------>

<?php

/* -----------------------------------------------------------   Gestión de recursos a usar.   -----------------------------------------------------------*/

require_once __DIR__ ."/../database/IEntity.php"; // Hará falta esta interfaz para ciertos procesos.

/* -----------------------------------------------------------   Definición y métodos de la clase.   -----------------------------------------------------------*/

// Esta clase es la que define las categorías de nuestras imágenes. 
class Categoria implements IEntity
{

    private $id; 

    private $nombre; 

    public function __construct( $id = 0, string $nombre = " ")
    {
        
        $this->id = $id;

        $this->nombre = $nombre;
        
    }

    public function toArray(): array
    {

        return 
        [

            "id"=>$this->getId(),

            "nombre"=>$this->getNombre()

        ];

    }
    
/* -----------------------------------------------------------   Getter and Setter.   -----------------------------------------------------------*/

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

}