<!------------------------------------------------------------   Clase para guardar mensajes.   ------------------------------------------------------------>

<?php

/* -----------------------------------------------------------   Gestión de recursos a usar.   -----------------------------------------------------------*/

require_once __DIR__ ."/../database/IEntity.php"; // Hará falta esta interfaz para ciertos procesos.

/* -----------------------------------------------------------   Definición y métodos de la clase.   -----------------------------------------------------------*/

class Consulta implements IEntity
{

    private $id; 

    private $nombre;
    
    private $apellidos;

    private $asunto; 

    private $pregunta; 

    private $email;


    public function __construct( $id = 0, string $nombre = " ", string $apellidos = " ", string $asunto = " ",
    string $pregunta = " ", string $email = " ")
    {

        $this->id = $id; 

        $this->nombre = $nombre;

        $this->apellidos = $apellidos;

        $this->asunto = $asunto;

        $this->pregunta = $pregunta;

        $this->email = $email;

    }

    public function toArray(): array
    {

        return
        [

            "id"=>$this->getId(), 

            "nombre"=>$this->getNombre(),

            "apellidos"=>$this->getApellidos(),

            "asunto"=>$this->getAsunto(),

            "pregunta"=>$this->getPregunta(),

            "email"=>$this->getEmail()

        ];
    }

/* -----------------------------------------------------------   Getter and Setter.   -----------------------------------------------------------*/

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
    

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of apellidos
     */ 
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set the value of apellidos
     *
     * @return  self
     */ 
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get the value of asunto
     */ 
    public function getAsunto()
    {
        return $this->asunto;
    }

    /**
     * Set the value of asunto
     *
     * @return  self
     */ 
    public function setAsunto($asunto)
    {
        $this->asunto = $asunto;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of pregunta
     */ 
    public function getPregunta()
    {
        return $this->pregunta;
    }

    /**
     * Set the value of pregunta
     *
     * @return  self
     */ 
    public function setPregunta($pregunta)
    {
        $this->pregunta = $pregunta;

        return $this;
    }
}