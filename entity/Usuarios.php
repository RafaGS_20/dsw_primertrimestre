<!------------------------------------------------------------   Clase para gestión de usuarios.   ------------------------------------------------------------>

<?php

/* -----------------------------------------------------------   Gestión de recursos a usar.   -----------------------------------------------------------*/

require_once __DIR__ ."/../database/IEntity.php"; // Hará falta esta interfaz para ciertos procesos.

/* -----------------------------------------------------------   Definición y métodos de la clase.   -----------------------------------------------------------*/

class Usuario implements IEntity
{

    private $id; 

    private $nombre; 

    private $contraseña; 

    private $rol; 

    public function __construct( $id = 0, string $nombre = " ", string $contraseña = " ", string $rol = "Visitante")
    {

        $this->id = $id; 

        $this->nombre = $nombre;

        $this->contraseña = $contraseña;

        $this->rol = $rol; 

    }

    public function toArray(): array
    {

        return
        [

            "id"=>$this->getId(), 

            "nombre"=>$this->getNombre(),

            "contraseña"=>$this->getContraseña(), 

            "rol"=>$this->getRol()

        ];
    }

/* -----------------------------------------------------------   Getter and Setter.   -----------------------------------------------------------*/

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of contraseña
     */ 
    public function getContraseña()
    {
        return $this->contraseña;
    }

    /**
     * Set the value of contraseña
     *
     * @return  self
     */ 
    public function setContraseña($contraseña)
    {
        $this->contraseña = $contraseña;

        return $this;
    }

    /**
     * Get the value of rol
     */ 
    public function getRol()
    {
        return $this->rol;
    }

    /**
     * Set the value of rol
     *
     * @return  self
     */ 
    public function setRol($rol)
    {
        $this->rol = $rol;

        return $this;
    }
}