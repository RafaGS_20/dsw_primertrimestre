<!------------------------------------------------------------   Clase para las imágenes de la página.   ------------------------------------------------------------>

<?php

/* -----------------------------------------------------------   Gestión de recursos a usar.   -----------------------------------------------------------*/

require_once __DIR__ ."/../database/IEntity.php"; // Hará falta esta interfaz para ciertos procesos.

/* -----------------------------------------------------------   Definición y métodos de la clase.   -----------------------------------------------------------*/

// Clase que define todas las imágenes de nuestra galería. 
class ImagenPagina implements IEntity
{
    private $id;
    
    private $nombre;

    private $categoria;

    private $descripcion;

    const RUTA_IMAGENES_PORTFOLIO = "img/portfolio/";
    const RUTA_IMAGENES_GALLERY = "img/gallery/";

    public function __construct($id = 0, $nombre = "", $descripcion = "", $categoria = "")
    {
        $this->id = $id;

        $this->nombre = $nombre;

        $this->descripcion = $descripcion;

        $this->categoria = $categoria;

    }

    public function toArray(): array
    {
        return [

            "id"=>$this->getId(), 

            "nombre"=>$this->getNombre(),

            "descripcion"=>$this->getDescripcion(),
            
            "categoria"=>$this->getCategoria(),
            
        ];
    }

/* -----------------------------------------------------------   Getter and Setter.   -----------------------------------------------------------*/

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of categoria
     */ 
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set the value of categoria
     *
     * @return  self
     */ 
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get the value of descripcion
     */ 
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set the value of descripcion
     *
     * @return  self
     */ 
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getURLPortfolio(): string
    {
        return self::RUTA_IMAGENES_PORTFOLIO . $this->getNombre();
    }

    public function getURLGallery(): string
    {
        return self::RUTA_IMAGENES_GALLERY . $this->getNombre();
    }
    
}

