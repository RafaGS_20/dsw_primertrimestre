<!------------------------------------------------------------   Gestión de archivos para subir.   ------------------------------------------------------------>

<?php

/* -----------------------------------------------------------   Definición y métodos de la clase.   -----------------------------------------------------------*/

//Clase que crearemos para la subida de archivos. 
class File
{

    private $file;

    private $fileName;

    public function __construct(string $fileName, array $arrTypes)
    {
        $this->file = $_FILES[$fileName];

        $this->fileName = "";

        if (($this->file["name"] == "")) {
            throw new FileException("Debes especificar un fichero.");
        }
        
        if ($this->file["error"] != UPLOAD_ERR_OK) {
            switch ($this->file["error"]) {
                case UPLOAD_ERR_INI_SIZE:

                case UPLOAD_ERR_FORM_SIZE:

                    throw new FileException("Tamaño del archivo no válido.");

                case UPLOAD_ERR_PARTIAL:

                    throw new FileException("Archivo corrupto.");


                default:

                    throw new FileException("Ups, algo ha salido mal.");

                    break;
            }
        }

        if (in_array($this->file["type"], $arrTypes) === false) {
            throw new FileException("Formato inválido.", 5);
        }
    }

    public function saveUploadFile( string $ruta)
    {        
        $rutaDestino = $ruta.$this->file["name"];

        if ( is_uploaded_file($this->file["tmp_name"] == false) )
        {
            throw new FileException("Archivo no subido por formulario.");
        }

        if ( is_file( $rutaDestino ) )
        {
            $idUnico = time();
            $this-> file["name"]= $idUnico.$this->file["name"]; 
            $rutaDestino = $ruta.$this->file["name"];
        }
        

        if ( move_uploaded_file( $this->file['tmp_name'], $rutaDestino) == false)
        {
            throw new FileException("No se ha podido mover el archivo.");
        }
    }

    public function copyFile ($rutaOrigen, $rutaCopiar) 
    {
        if ( is_file( $rutaOrigen.$this->file["name"] ) == false )
        {
            throw new FileException( "No existe el fichero en ".$rutaOrigen );
        }
        if ( is_file( $rutaCopiar.$this->file["name"] ))
        {
            throw new FileException( "Ya existe el fichero en ".$rutaOrigen );
        } else 
        {
            if (copy($rutaOrigen.$this->file["name"], $rutaCopiar.$this->file["name"]) == false) 
            {
                throw new FileException ( "No se ha podido copiar el fichero." );
            }
        }
    }

/* -----------------------------------------------------------   Getter and Setter.   -----------------------------------------------------------*/

    /**
     * Get the value of fileName
     */
    public function getFileName()
    {
        return $this->file["name"];
    }

}