<!------------------------------------------------------------   Control clase activa en el nav.   ------------------------------------------------------------>

<?php

/* -----------------------------------------------------------   Definición y métodos de la clase.   -----------------------------------------------------------*/

// Este php solo incorpora una función para determinar que elemento del nav está "activo". 
function esactiva ($pagina) {

    $documento = $_SERVER['REQUEST_URI'];
    if (strpos($documento, $pagina)  > 0) {
        return true;
    } else {
        return false;
    }
}

?>

