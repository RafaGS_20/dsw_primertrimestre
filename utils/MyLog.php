<!------------------------------------------------------------   Gestión de logs.   ------------------------------------------------------------>

<?php

/* -----------------------------------------------------------   Gestión de recursos a usar.   -----------------------------------------------------------*/

// Monolog: uso del monolog para la gestión de logs en nuestro servicio. 
use Monolog\Logger; // Se usa para la creación y manejos de logs.
use Monolog\Handler\StreamHandler; // Se usa para almacenar recursos en local. 

/* -----------------------------------------------------------   Definición y métodos de la clase.   -----------------------------------------------------------*/

// Clase MyLog, la usaremos para incorporarla por toda nuestra página y gestionar los logs. 
class MyLog

{

    private $log; 

    private function __construct (string $filename) {

        $this->log = new Logger ('DSW');

        $this->log->pushHandler(new StreamHandler ($filename, Logger::INFO) );

    }

    public static function load (string $filename) {

        return new MyLog($filename);

    }

    public function add (string $message) {

        $this->log->info($message);

    }

}

?>