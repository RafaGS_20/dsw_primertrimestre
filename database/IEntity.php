<!------------------------------------------------------------   Mecanismo to array.   ------------------------------------------------------------>

<?php 

/* -----------------------------------------------------------   Definición y métodos de la clase.   -----------------------------------------------------------*/

/* Definición de interface: una interfaz viene a ser los requisitos mínimos
que debe de tener cualquier clase que lo englobe. Si definimos un método aquí
será obligatorio que cualquier clase que tenga esta interfaz tenga ese método.
Es una plantilla para todas nuestras clases. */ 

// Interface IEntity, la usaremos en todos los modelos. 
interface IEntity {

    /* Este método coge un objeto y lo transforma en array. Lo usaremos para
    coger los objetos, pasarlos a array y meter ese array en la base de datos. */
    public function toArray(): array;
    
}
?>