<!------------------------------------------------------------   Mecanismo de consultas.   ------------------------------------------------------------>

<?php

/* -----------------------------------------------------------   Gestión de recursos a usar.   -----------------------------------------------------------*/

require_once __DIR__ ."/../exceptions/QueryException.php"; // Clase que maneja los errores producidos durante la interacción con la base de datos.

require_once __DIR__ ."/../exceptions/NotFoundException.php"; // Clase que maneja los errores producidos durante la interacción con la base de datos.

require_once __DIR__ ."/../core/App.php"; // Clase que gestiona el contenedor de servicio.

require_once __DIR__ ."/IEntity.php"; // Interfaz que nos fuerza a usar un determinado método. 



// Clase que define las interacciones con la base de datos. 
/* La inyección de dependencias es pasarle a la clase, a través del 
contrusctor, de forma directa todo lo que va a necesitar para funcionar. */
/* Clase abstracta: es una clase que necesita de otra clase secundaria
para ejecutar las tareas que tiene programadas en sus métodos. */

// Clase que define las interacciones con la base de datos. 
abstract class QueryBuilder 

{
    // Conexión. 
    private $connection; 
    // Tabla de la base de datos. 
    private $table;
    // Nombre del modelo que le vamos a pasar. 
    private $classEntity;

    // Realizar la conexión. 
    public function __construct(string $table, string $classEntity)
    {   

        $this->connection = App::getConnection(); 

        $this->table = $table;

        $this->classEntity = $classEntity;

    }

    // Función creada para ejecutar consultas. 
    public function executeQuery(string $sql): array 
    {
   
      $pdoStatement = $this->connection->prepare($sql);
        
       if ($pdoStatement->execute()===false)
            
           throw new QueryException("No se ha podido ejecutar la consulta.");
    
       return $pdoStatement->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $this->classEntity); 
       
    }

    // Función creada para encontrar todos los datos de cierta tabla. 
    public function findAll ()
    {
        $sql = "SELECT * FROM $this->table";
        
        // Incluimos el método creado executeQuery para limpiar código. 
        $result = $this->executeQuery($sql);

        return $result;
        
    }

    // Función creada para hacer las inserciones en nuestra base de datos. 
    public function save(IEntity $entity): void
    {
        try 
        {
            $parameters = $entity->toArray();
            
            $sql = sprintf("insert into %s (%s) values (%s)",
            
            $this->table, 

            implode(", ", array_keys($parameters)), 

            ":".implode(", :",array_keys($parameters))

            ); 

            $statement = $this->connection->prepare($sql); 

            $statement->execute($parameters);


        } catch (PDOException $exception) {

            throw new QueryException("Error al insertar en la BBDD.");

        }
    }

    // Función creada para devolver un registro concreto de nuestra base. 
    public function find (int $id): IEntity
    {

        $sql = "SELECT * FROM $this->table WHERE id=$id"; 

        $result = $this->executeQuery($sql);

        if(empty($result))
        
            throw new NotFoundException("No se ha encontrado el elemento con id $id");

        return $result[0];

    }
}

?>