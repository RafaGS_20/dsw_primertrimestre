<!------------------------------------------------------------   Mecanismo de conexión a la base de datos.   ------------------------------------------------------------>

<?php

/* -----------------------------------------------------------   Definición y métodos de la clase.   -----------------------------------------------------------*/

// Creamos la clase Connection (conexión) que será la que usemos para entrar a la BBDD. 
class Connection {
    /* Aquí creamos el método que nos permitirá conectarnos. Al pasarle a la función el
    config como parámetro, le estaremos enviando toda la configuración que tenemos en el 
    archivo. */
    public static function make($config) 
    {

        try {
            /* Llamamos al método get de App.php para obtener la conexión. */ 
            $config = App::get("config")["database"];
            /* Creamos un objeto PDO para gestionar la conexión. En ella le pasamos los 
            parámetros guardados en $config. */
            $connection = new PDO (
                // "mysql:host=localhost" . ";dbname = " . "proyecto" 
                $config["connection"] . ";dbname=" . $config["name"],
                // rafa 
                $config["username"], 
                // contraseña
                $config["password"],
                // Las opciones definidas en el doc. 
                $config["options"]
            );

        }
        // Pilla los fallos del try. 
        catch (PDOException $PDOException)
        {
            throw new AppException("No se ha podido conectar con la BBDD.");
        }
        // Devuelve la conexión.
        return $connection;
    }

}
