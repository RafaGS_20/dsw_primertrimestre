<!------------------------------------------------------------   Gestor de archivo solicitado.   ------------------------------------------------------------>


<?php 

/* -----------------------------------------------------------   Gestión de recursos a usar.   -----------------------------------------------------------*/

// require "core/bootstrap.php"; // Llamamos al archivo que tiene la configuración.
require_once __DIR__ . "/core/Request.php";

/* -----------------------------------------------------------   Funcionalidad de la página.   -----------------------------------------------------------*/

// Llamamos a nuestro enrutador cargándolo en una variable. 
$routes = require "app/routes.php";

// Llamamos al método creado en la clase Request. 
require $routes[Request::uri()];

?>