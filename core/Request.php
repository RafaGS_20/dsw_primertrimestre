<!------------------------------------------------------------   Gestor de URL solicitada.   ------------------------------------------------------------>

<?php

/* -----------------------------------------------------------   Definición y métodos de la clase.   -----------------------------------------------------------*/

/* Esta clase gestionará las url para ayudarnos a conventirlas en amigables y 
mandarlas al enrutador de forma útil. */

class Request 
{

    public static function uri() 
    {

        return trim(parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH), "/");

    }

}