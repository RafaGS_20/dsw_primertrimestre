<!------------------------------------------------------------   Contenedor de servicios.   ------------------------------------------------------------>

<?php 

/* -----------------------------------------------------------   Definición y métodos de la clase.   -----------------------------------------------------------*/

// Clase que gestiona los datos y código a reutilizar en nuestra app. 
// Esta clase será un contenedor de servicios. 
class App {
    // Almacena los datos en nuestro contenedor. 
    private static $container = [];

    /* El siguiente método mete cosas en el array volviendolo asociativo. 
    Para cada clave habrá un valor. */
    public static function bind (string $key, $value)
    {
        static::$container[$key] = $value;
    }
    // Creamos también un método get para sacar datos del array. 
    public static function get (string $key)
    {
        // Si en el array no se encuentra la clave: 
        if (!array_key_exists($key, static::$container))
            throw new AppException("No se ha encontrado la clave $key en el contenedor.");
        // En caso de encontrarse devuelve el valor. 
        return static::$container[$key];
    }
    // Método que devuelve la conexión con la base de datos y si no existe la crea. 
    public static function getConnection()
    {
        // En caso de no existir la conexión, se crea. 
        if (!array_key_exists("connection", static::$container))
        {
            $config = require_once("app/config.php");
            static::$container["connection"] = Connection::make($config);
        }
        return static::$container["connection"];
    }
}

?>