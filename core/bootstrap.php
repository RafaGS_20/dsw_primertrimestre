<!------------------------------------------------------------   Sesiones y otras configuraciones.   ------------------------------------------------------------>

<?php

/* -----------------------------------------------------------   Gestión de recursos a usar.   -----------------------------------------------------------*/

require_once "App.php"; // Llamamos a nuestro contenedor de servicios. 
require_once "Request.php"; // Llamamos a nuestro trimeador de url.
require_once "vendor/autoload.php"; // Se realizan las cargas del composer. 
require_once "utils/MyLog.php"; // Archivo creado para monolog. 

/* -----------------------------------------------------------   Funcionalidad de la página.   -----------------------------------------------------------*/

// Uso de sesiones para el almacenamiento de errores por cada usuario. 
session_start();
/* En caso de ya haber sido inicializada el método read que incluye el session_start leerá la información de la sesión ya iniciada y nos permitirá
seguir trabajando con esta misma sin romper su funcionamiento.  */ 

// Uso de monolog. 
$logger = MyLog::load("logs/info.log");

// Asociamos la configuración cargada para usarla en toda la aplicación. 
App::bind("logger", $logger);

// Almacenamos toda la configuración en una variable. 
$config = require_once __DIR__ . "/../app/config.php";

// Asociamos la configuración cargada para usarla en toda la aplicación. 
App::bind("config", $config);

?>