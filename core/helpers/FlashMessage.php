<!------------------------------------------------------------   Persistencia de datos.   ------------------------------------------------------------>

<?php

/* -----------------------------------------------------------   Definición y métodos de la clase.   -----------------------------------------------------------*/

/* Esta clase es creada para mantener la persistencia de datos en los formularios. Al crearla por separado cumple con dos funciones: 
    1. La primera de ellas es que la podemos usar por todas las páginas de nuestra aplicación. 
    2. Implementamos un POST-Redirect-Get-Flash para evitar los post dobles. */

class FlashMessage
{

    public static function get(string $key, $default="")
    {

        if (isset($_SESSION["flash-message"])) {

            $value = $_SESSION["flash-message"][$key] ?? $default;

            unset($_SESSION["flash-message"][$key]); // Para no repetir los mensajes. 

        }

        else 

            $value = $default;

        return $value;

    }

    public static function set(string $key, $value)
    {

        $_SESSION["flash-message"][$key] = $value;

    }

    public static function unset (string $key)
    {

        if (isset($_SESSION["flash-message"]))

        unset($_SESSION["flash-message"][$key]);

    }

}
