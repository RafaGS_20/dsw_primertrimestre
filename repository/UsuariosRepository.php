<!------------------------------------------------------------   Interacción con la base de datos.   ------------------------------------------------------------>

<?php 

/* -----------------------------------------------------------   Gestión de recursos a usar.   -----------------------------------------------------------*/

require_once __DIR__ ."/../database/QueryBuilder.php"; // Requerimos el querybuilder creado para interactuar con la base de datos. 

/* -----------------------------------------------------------   Definición y métodos de la clase.   -----------------------------------------------------------*/

// Esta clase sería el querybuilder creado para alterar únicamente la tabla categorías.
class UsuariosRepository extends QueryBuilder
{

    public function __construct(string $table="usuarios", string $classEntity="Usuarios")
    {
        parent::__construct($table, $classEntity);
    }
}

?>